#!/bin/bash
APP_NAME=client-jar-etlcloud.jar

# ======================================== PARAMETERS =================================================
# DSN Central Manager etlcloud.io
CLIENT_DSN=https://00000000000000000000000000000000@api.pentafilestore.com/pipeline/v1

# Public IP and Port,Open the port for public listening, replace 'localhost' by public IP
CLIENT_ENDPOINT=http://localhost:9700

# Default connector to Carte,If you do not want to make special settings, do not modify
CLIENT_CARTE=http://cluster:cluster@localhost:9001

# Kettle, you can download here https://sourceforge.net/projects/pentaho/files/Data%20Integration/
CLIENT_PDI=/opt/pentaho/data-integration

# Folder for the projects to be managed
CLIENT_FOLDER=/opt/jobs
#=====================================================================================================

# Check client
if [ -f $APP_NAME ]; then
else
        wget -O $APP_NAME https://gitlab.com/rclaros/etlcloud-packages/client-v1-etcloud.jar
        echo "Client download succefull OK"
fi

usage() {
    echo "Uso: sh ejecutar script.sh [inicio | detener | reiniciar | estado]"
    exit 1
}

is_exist(){
  pid=`ps -ef|grep $APP_NAME|grep -v grep|awk '{print $2}' `
  #Si no hay retorno 1, devuelve 0     
  if [ -z "${pid}" ]; then
   return 1
  else
    return 0
  fi
}

start(){
  is_exist
  if [ $? -eq "0" ]; then
    echo "${APP_NAME} is already running. pid=${pid} ."
  else
    nohup java -jar $APP_NAME --dsn=$CLIENT_DSN --carte=$CLIENT_CARTE --endpoint=$CLIENT_ENDPOINT --pdi=$CLIENT_PDI --folder=$CLIENT_FOLDER > /dev/null 2>&1 &
  fi
}

stop(){
  is_exist
  if [ $? -eq "0" ]; then
    kill -9 $pid
  else
    echo "${APP_NAME} is not running"
  fi  
}

status(){
  is_exist
  if [ $? -eq "0" ]; then
    echo "${APP_NAME} is running. Pid is ${pid}"
  else
    echo "${APP_NAME} is NOT running."
  fi
}

restart(){
  stop
  start
}

case "$1" in
  "start")
    start
    ;;
  "stop")
    stop
    ;;
  "status")
    status
    ;;
  "restart")
    restart
    ;;
  *)
    usage
    ;;
esac

